# Cribl Source Pack for Palo Alto Network Firewalls
----

This Cribl Source Pack for Palo Alto Network Firewalls process Traffic and Threat logs and maps them into the Cribl Common Schema.  This pack is designed to be applied to the source where the Palo Alto logs are coming in.

## Installation

1. Install this pack from the [Cribl Pack Dispensary](https://packs.cribl.io), use the Git clone feature inside Cribl Stream, or download the most recent .crbl file from the repo
2. Apply the Pack to the Source. (i.e., Syslog Source input)

This pack assumes that the Palo Alto logs are coming in via Syslog and CSV format.  

## Release Notes

### Version 1.0.0 - 2023-12-13
Initial Release supporting Palo Alto Traffic and Threat Logs

## Contributing to the Pack
Discuss this pack on our Community Slack channel [#packs](https://cribl-community.slack.com/archives/C021UP7ETM3).

## Contact
The author of this pack is Dan Schmitz and can be contacted at <dschmitz@cribl.io>.


